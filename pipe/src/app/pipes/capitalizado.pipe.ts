import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  //capitalizado | true
  transform(value: string, todas: boolean = true ): string {
    value = value.toLocaleLowerCase();
    //split -->  convierte en un arreglo, a cada elemento despues despues de un spacio.
    let nombres = value.split(' ');

    if( todas ){
      nombres = nombres.map( nombre => {
        return nombre[0].toLocaleUpperCase() + nombre.substr(1);
        //nombre[0].toLocaleUpperCase() --> mayuscula la primera letra
        //nombre.substr(1)  --> obtiene a partir de la posicion 1
      });

    }else {
      // solo lo primera palabra capitalizada
      nombres[0] = nombres[0][0].toLocaleUpperCase() + nombres[0].substr(1);
    }
    // unimos un arreglo es un solo string
    return nombres.join(' ');

  }

}
