import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre: string = "Capitan America";

  nombre2: string = 'ElvIs HuAcaChi cAldAs';

  personajes = ['Elvis', 'Claudio', 'Huacachi', 'Caldas', 'Elvis', 'Huacachi', 'Caldas'];

  PI: number = Math.PI;

  porcentaje: number = 0.235;

  salario: number = 12345.5;

  fecha: Date = new Date();

  idioma: string = 'en';

  videoUrl: string = 'https://www.youtube.com/embed/D-DGIas05xM';

  activar:  boolean = true;



  heroe = {
    nombre: 'Logan',
    clave: 'Volverine',
    edad: '500',
    direccion: {
      calle: 'Primavera',
      casa: 20
    }
  }

  //emite un String, luego de 4,5 segundos
  valorPromesa = new Promise<string>( (resolve) => {
    setTimeout( () => {
      resolve('Llego la data');
    }, 4500);
  });
}
